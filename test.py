import os
import reframe as rfm
import reframe.utility.sanity as sn

# No need to decorate this class because we will not run it
# independently.
class build_myapp(rfm.CompileOnlyRegressionTest):
    build_system = 'CMake'
    sourcesdir = 'myapp'

@rfm.simple_test
class myapp_build_test(rfm.RunOnlyRegressionTest):
    valid_systems = ['*']
    valid_prog_environs = ['+boost.program_options +cxx']
    dofs = variable(int, value=1000)
    # Run this test for each one of these parameter values
    #
    # TODO make this go from 1 to nproc.
    num_cores = parameter([1,2,3,4,5,6])

    # We require the build step as a fixture so that the binary is
    # built (once) before the execution of this step.
    binary = fixture(build_myapp, scope='environment')
    # TODO
    # reference should change according to parameters of the binary
    # (processes and dofs.)
    reference = {
        'tutorialsys:default': {
            'total_time': (23_890, -0.10, 0.30, 's'),
        }
    }

    @run_after('setup')
    def set_executable(self):
        self.executable = os.path.join(self.binary.stagedir,
                                       'myapp')
        # This is how to pass arguments to an executable
        # (executable_opts can also be set outside in the class
        # definition.)
        self.executable_opts = ['--weak', '--dofs', str(self.dofs), '--processes', str(self.num_cores)]

    @sanity_function
    def validate(self):
        return sn.assert_found(r'^((?!error).)*$', self.stdout)

    @performance_function('s')
    def total_time(self):
        return sn.extractsingle(r'time:\s+(\S+)', self.stdout, 1, float)
