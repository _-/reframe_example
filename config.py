site_configuration = {
    # The list of systems
    #
    # A system is an abstraction of an HPC system. If your tests are
    # meant to run on multiple unrelated HPC clusters, e.g. OLCF-5and
    # Fugaku, it is likely that you will need different configurations
    # for them as they have different setups.
    'systems': [
        # System #1
        {
            'name': 'tutorialsys',
            'descr': 'Example system',
            # The hostnames for this system.
            #
            # If your node hostname does not match this list, the
            # tests will not be run.
            'hostnames': ['debian'],
            # A partition is a group of nodes that provide the same
            # group of environments.
            #
            # For example, the 'default' partition defined below is
            # for nodes that have the tools to support two
            # environments, the 'baseline' (this is just illustrative)
            # and the 'gnu' (think gcc toolchain) environment.
            'partitions': [
                {
                    'name': 'default',
                    'descr': 'Example partition',
                    'scheduler': 'local',
                    'launcher': 'local',
                    'environs': ['baseline', 'gnu']
                }
            ]
        }
        # Add more systems below ... 
    ],
    'environments': [
        {
            'name': 'baseline',
            # This is an imaginary feature that we named stream.
            'features': ['stream']
        },
        {
            # This environment defines the C compiler to be gcc and
            # the C++ compiler to be g++.
            'name': 'gnu',
            'cc': 'gcc',
            'cxx': 'g++',
            # TODO
            # As it stands, boost.program_options doesn't actually
            # require the node to have the library installed. I am not
            # sure how to connect this feature to an actual
            # requirement of the library being present.
            'features': ['boost.program_options', 'cxx'],
        },
    ]
}
