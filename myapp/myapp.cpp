#include <boost/program_options.hpp>
#include <cstdlib>
#include <iostream>
#include <ostream>

namespace po = boost::program_options;

void option_conflict(const po::variables_map &vm, const std::string &first,
                     const std::string &second) {
  if (vm.count(first) && vm.count(second))
    throw std::logic_error("Option '" + first + "' conflicts with '" + second +
                           "'.");
}

int main(int argc, char **argv) {
  try {
    po::options_description desc("Allowed options");
    // clang-format off
    desc.add_options()
      ("help,h", "produce help message")
      ("weak,W", "use weak scaling")("strong,S", "use strong scaling")
      ("processes,p", po::value<unsigned>()->value_name("PROCESSES")->default_value(1),
        "number of processes to use")
      ("dofs,d", po::value<unsigned>()->value_name("DOFS")->default_value(10000),
        "number of degrees of freedom to use");
    // clang-format on
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    option_conflict(vm, "weak", "strong");
    po::notify(vm);
    if (vm.count("help")) {
      std::cout << "Mock program for examining weak and strong scaling."
                << std::endl
                << std::endl;
      std::cout << desc << std::endl;
      return EXIT_SUCCESS;
    }
    unsigned processes = vm["processes"].as<unsigned>();
    unsigned dofs = vm["dofs"].as<unsigned>();
    bool weak = !vm.count("strong");
    if (weak == false) {
      dofs *= processes;
    }
    std::cout << "====== Performance metrics ======" << std::endl;
    std::cout << "Using " << (weak ? "weak" : "strong") << " scaling."
              << std::endl;
    if (weak == true) {
      std::cout << "Processes: " << processes
                << " with dofs per process: " << dofs << std::endl;
    } else {
      std::cout << "Processes: " << processes << " with total dofs: " << dofs
                << std::endl;
    }
    std::cout << "Total time: " << 1000 + (123.456 * dofs) / processes << " s."
              << std::endl;
  } catch (std::exception &e) {
    std::cerr << "error: " << e.what() << std::endl;
    return EXIT_FAILURE;
  } catch (...) {
    std::cerr << "Exception of unknown type!" << std::endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
